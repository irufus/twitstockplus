#!/usr/bin/env python
# encoding: utf-8

# twitstock.py
# Get the value of a given stock item via
# Yahoo! Finance and post it to Twitter

import urllib2
import sys
from datetime import datetime, timedelta
import twitter
from access import * # oauth secrets

# Yahoo! Finance URI
# f=b2: asking Y! to retrive realtime information
# s=??: stock symbol
YURL="http://download.finance.yahoo.com/d/quotes.csv?f=b2&s="

# get the current stock value of a given company
def get_stock_value(symbol='FB'):
        try:
                result = urllib2.urlopen(YURL + symbol).read()
                return float(result)
        except:
                print "Error: Couldn't get stock value for %s" % symbol
                return "N/A"

# post the current value of the given symbol to twitter
def post_stock_value_to_twitter(value, symbol='FB'):
        # check if the value is convertable to float
        # and therefor a valid value...
        try:
                float(value)
        except: # ...otherwise just die
                print "Error: Stock value is invalid, must be float."
                sys.exit(0)

        # post to Twitter
        try:
                api = twitter.Api(consumer_key=CONSUMER_KEY, consumer_secret=CONSUMER_SECRET, access_token_key=ACCESS_TOKEN, access_token_secret=ACCESS_TOKEN_SECRET)
                api.VerifyCredentials()
                api.PostUpdate("Current (%s UTC+2) value of $%s is %.2f." % (datetime.now().strftime("%d/%m/%Y %H:%M:%S"), symbol, value))
        except twitter.TwitterError, e:
                print "Error: %s" % e
        except:
                sys.exit(0)

if __name__ == "__main__":
		value = get_stock_value()
		symbol = "FB"
		f = None

		# check if we've an old value of the symbol
		try:
				f = open("/tmp/%s_stock" % symbol, "r")
				if float(f.readline()) == value:
						f.close()
						sys.exit(0)
		except IOError:
				print "Error: Couldn't get old value of symbol."
		finally:
				if f:
						f.close()
				
		post_stock_value_to_twitter(value, symbol)
	
		try:
				f = open("/tmp/%s_stock" % symbol, "w")
				f.write(str(value))
		except IOError:
				print "Error: Couldn't update file with stock value."
		finally:
				if f:
						f.close()					
								
